const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs')
const config = require('config')
const auth = require('../middleware/auth')
const jwt = require('jsonwebtoken')

const User = require('../models/User')


// @router      GET api/auth
// @desc        Get logged in a user
// @access      Private
router.get('/', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password')
        res.json(user)
    } catch (error) {
        console.log(error.message)
        res.status(500).send('Server Error')
    }
})


// @router      POST api/auth
// @desc        Auth user & get token
// @access      Public
router.post('/',
    [
        check('email', 'Please include a valid email')
            .isEmail(),
        check('password', 'Please enter a password with 6 or more characters')
            .exists()

    ],
    async (req, res) => {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
        }

        const { email, password } = req.body

        try {
            let user = await User.findOne({ email: email })
            if (!user) {
                return res.status(400).json({ msg: 'Invalid credentials' })
            }
            const isMatch = await bcrypt.compare(password, user.password)

            if (!isMatch) {
                return res.status(400).json({ msg: 'Invalid credentials' })
            }


            const payload = {
                user: {
                    id: user.id
                }
            }

            jwt.sign(payload, config.get('jwtSecret'), {
                expiresIn: 360000
            }, (err, token) => {
                if (err) throw err;
                res.json({ token: token })
            })


        } catch (error) {
            console.log(error.message)
            res.status(500).send('Server Error')

        }
    })


module.exports = router;